//
//  ParkingDetailViewController.swift
//  ParkingSpaces
//
//  Created by Lo Sheng Peng on 2019/2/18.
//  Copyright © 2019 Lo Sheng Peng. All rights reserved.
//

import UIKit
import MapKit
class ParkingDetailViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    public var parkingSpace:ApiModel.QueryAll.Response.Record?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell
        if indexPath.row == 4 {
            let mapCell = tableView.dequeueReusableCell(withIdentifier: "ParkingMapTableViewCell", for: indexPath) as! ParkingMapTableViewCell
            mapCell.mapView?.delegate = self

            if  let x = parkingSpace?.TW97X,
                let y = parkingSpace?.TW97Y,
                let doubleX = Double(x),
                let doubleY = Double(y) {
                let coordinate = CLLocationCoordinate2D.TranformTWD97( twd97x: doubleX,twd97y: doubleY)
                let region =  MKCoordinateRegion.init(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.007, longitudeDelta: 0.007))
                mapCell.mapView?.setRegion(region, animated: true)
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = coordinate
                mapCell.mapView?.addAnnotation(annotation)
            }
            cell = mapCell
        }
        else{
            let detailCell = tableView.dequeueReusableCell(withIdentifier: "ParkingDetailTableViewCell", for: indexPath) as! ParkingDetailTableViewCell
            switch indexPath.row{
            case 0:
                detailCell.labelLeft.text = "名稱";
                detailCell.labelRight.text = parkingSpace?.NAME;
            case 1:
                detailCell.labelLeft.text = "區域";
                detailCell.labelRight.text = parkingSpace?.AREA;
            case 2:
                detailCell.labelLeft.text = "營業時間";
                detailCell.labelRight.text = parkingSpace?.SERVICETIME;
            case 3:
                detailCell.labelLeft.text = "地址";
                detailCell.labelRight.text = parkingSpace?.ADDRESS;
            default:
                break
            }
            
            cell = detailCell
        }
        
        
        return cell
    }
    
    
}
