//
//  ApiProtocol.swift
//  JobSearch
//
//  Created by Joe Lo on 2018/7/11.
//  Copyright © 2018年 Joe Lo. All rights reserved.
//

import Foundation

protocol ApiRequest:Encodable{}

protocol ApiResponse:Decodable{}

extension ApiRequest
{
    func jsonData() -> Data?
    {
        return try?  JSONEncoder().encode(self)
    }
    func dic() -> [String: Any]?
    {
        guard let data = self.jsonData(),
            let dic = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
            else{
                return nil
        }
        return dic
    }
    func formDataString() -> String?
    {
        return self.dic()?.map { "\($0)=\($1)" }.joined(separator: "&") ?? nil
    }
}
public enum ApiError:Equatable{
    case noError
    case parameterError(String)
    case requestError
    case decodeError
    case sessionError(String)
    public static func == (lhs: ApiError, rhs: ApiError) -> Bool
    {
        switch (lhs, rhs) {
        case (.noError,.noError),(.requestError,.requestError),(.decodeError,.decodeError):
            return true
        default:
            return false
        }
    }
}
public enum ApiHttpMethod {
    case get
    case post
}
protocol ApiFunctionProtocol
{
    static var ApiServer:String { get }
    static var ApiPath:String { get }
    static var HttpMethod:ApiHttpMethod { get }
    
    var request:Request? { get set }
    var response:Response? { get set }
    
    associatedtype Request:ApiRequest
    associatedtype Response:ApiResponse
}

protocol ApiProtocol {
    func sendApi<T:ApiFunctionProtocol>(function: T, completion: @escaping ((_ error:ApiError,_ result:T?) -> ()))
}
extension ApiProtocol {
    func sendApi<T:ApiFunctionProtocol>(function: T, completion: @escaping ((_ error:ApiError,_ result:T?) -> ()))
    {
        let FunctionType = type(of: function)
        var urlComponents = URLComponents(string: FunctionType.ApiServer)
        let path = urlComponents?.path ?? ""
        urlComponents?.path = path + FunctionType.ApiPath
        
        guard let parameter = function.request,
            let url = urlComponents?.url
            else
        {
            DispatchQueue.main.async {
                completion(.requestError, nil)
            }
            return
        }
        
        var urlRequest = URLRequest(url: url)
        
        switch FunctionType.HttpMethod {
        case .get:
            urlRequest.httpMethod = "GET"
            let items = parameter.dic()?.reduce([URLQueryItem](), { (queryItems, item) -> [URLQueryItem] in
                
                return queryItems + [URLQueryItem(name: item.key, value: "\(item.value)")]
            })
            urlComponents?.queryItems = items
            urlRequest.url = urlComponents?.url
            
        case .post:
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            if let dataString = parameter.formDataString()
            {
                urlRequest.httpBody = dataString.data(using: .utf8)
            }

        }
        
        URLSession.shared.dataTask(with: urlRequest) {(data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                DispatchQueue.main.async {
                    completion(.sessionError(error!.localizedDescription),nil)
                }
            }
            
            guard let data = data,
                let apiResponse = try? JSONDecoder().decode(FunctionType.Response.self, from: data)
                else {
                    DispatchQueue.main.async {
                        completion(.decodeError,nil)
                    }
                    return
            }
            DispatchQueue.main.async {
                var varFunction = function
                varFunction.response = apiResponse
                completion(.noError, varFunction)
                
            }
            
            
            }.resume()
    }
}
