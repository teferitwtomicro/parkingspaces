//
//  ParkingMapTableViewCell.swift
//  ParkingSpaces
//
//  Created by Lo Sheng Peng on 2019/2/19.
//  Copyright © 2019 Lo Sheng Peng. All rights reserved.
//

import UIKit
import MapKit

class ParkingMapTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
