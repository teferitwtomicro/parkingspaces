//
//  ParkingTableViewCell.swift
//  ParkingSpaces
//
//  Created by Lo Sheng Peng on 2019/2/19.
//  Copyright © 2019 Lo Sheng Peng. All rights reserved.
//

import UIKit

class ParkingTableViewCell: UITableViewCell {
    @IBOutlet weak var labelLeft1: UILabel!
    @IBOutlet weak var labelLeft2: UILabel!
    @IBOutlet weak var labelLeft3: UILabel!
    @IBOutlet weak var labelRight: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
