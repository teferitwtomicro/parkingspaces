//
//  ApiManager.swift
//  JobSearch
//
//  Created by Joe Lo on 2018/7/8.
//  Copyright © 2018年 Joe Lo. All rights reserved.
//

import Foundation

class ApiManager:ApiProtocol {
    
    
    static var shared = ApiManager()
    
    func sendQueryAllApi(completion:@escaping (ApiError ,ApiModel.QueryAll.Response?)->())
    {
        let function = ApiModel.QueryAll()
        
        sendApi(function: function) { (error, result) in
            completion(error, result?.response)
        }
    }

}
