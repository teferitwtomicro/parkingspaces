//
//  ApiModel.swift
//  JobSearch
//
//  Created by Joe Lo on 2018/7/9.
//  Copyright © 2018年 Joe Lo. All rights reserved.
//

import Foundation


struct ApiModel
{
    static let ApiServer = "http://data.ntpc.gov.tw/api/v1/rest/datastore/"
    
    struct QueryAll:ApiFunctionProtocol
    {
        static var ApiServer:String
        {
            return ApiModel.ApiServer
        }
        
        static let ApiPath: String = "382000000A-000225-002"
        
        static let HttpMethod: ApiHttpMethod = .get
        
        var request: Request? = Request()
        
        var response: Response?


        struct Request:ApiRequest
        {
            
        }
        struct Response:ApiResponse
        {
            struct Field:Codable {
                var type:String?
                var id:String?
            }
            struct Result:Codable {
                var records:[Record]?
                var resource_id:String?
                var limit:Int?
                var total:Int?
                var fields:[Field]?
                
            }
            struct Record:Codable
            {
                /*
                 {
                 ID: "010056",
                 AREA: "板橋區",
                 NAME: "遠東百貨停車場",
                 TYPE: "2",
                 SUMMARY: "立體式建築附設停車空間",
                 ADDRESS: "板橋區中山路一段152號",
                 TEL: "",
                 PAYEX: "小型車計時60元;",
                 SERVICETIME: "0~24時",
                 TW97X: "296882",
                 TW97Y: "2767068",
                 TOTALCAR: "453",
                 TOTALMOTOR: "0",
                 TOTALBIKE: ""
                 }
                 */
                var ID:String?
                var AREA:String?
                var NAME:String?
                var TYPE:String?
                var SUMMARY:String?
                var ADDRESS:String?
                var TEL:String?
                var PAYEX:String?
                var SERVICETIME:String?
                var TW97X:String?
                var TW97Y:String?
                var TOTALCAR:String?
                var TOTALMOTOR:String?
                var TOTALBIKE:String?
            }
            var success:Bool?
            var result:Result?        }
        
    }
}

