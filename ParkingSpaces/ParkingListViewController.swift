//
//  ViewController.swift
//  ParkingSpaces
//
//  Created by Lo Sheng Peng on 2019/2/17.
//  Copyright © 2019 Lo Sheng Peng. All rights reserved.
//

import UIKit

class ParkingListViewController: UITableViewController {
    var selectedIndex:Int?
    var parkingSpaces:[ApiModel.QueryAll.Response.Record]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setRefreshControl()
        loadData()
    }
    func setRefreshControl(){
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(loadData),
                                       for: .valueChanged)
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Data Refreshing...")
    }
    @objc func loadData(){
        ApiManager.shared.sendQueryAllApi {[weak self](error, response) in
            if error == .noError ,
                let records = response?.result?.records{
                self?.parkingSpaces = records
                self?.tableView?.reloadData()
                if let isRefreshing = self?.refreshControl?.isRefreshing,
                    isRefreshing{
                        self?.refreshControl?.endRefreshing()
                }
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parkingSpaces?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ParkingTableViewCell = tableView.dequeueReusableCell(withIdentifier:"ParkingTableViewCell", for: indexPath) as! ParkingTableViewCell
        let parkingSpace = parkingSpaces?[indexPath.row]
        cell.labelLeft1?.text = parkingSpace?.AREA
        cell.labelLeft2?.text = parkingSpace?.SERVICETIME
        cell.labelLeft3?.text = parkingSpace?.ADDRESS
        cell.labelRight?.text = parkingSpace?.NAME
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "toDetail", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let vc as ParkingDetailViewController:
            if let index = selectedIndex,
                let spaces = parkingSpaces{
                vc.parkingSpace = spaces[index]
            }
        default: break
        }
    }
    
}

